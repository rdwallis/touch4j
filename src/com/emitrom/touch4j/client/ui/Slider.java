/**
 Copyright (c) 2013 Emitrom LLC. All rights reserved.
 For licensing questions, please contact us at licensing@emitrom.com

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package com.emitrom.touch4j.client.ui;

import com.emitrom.touch4j.client.core.config.Event;
import com.emitrom.touch4j.client.core.config.SliderConfig;
import com.emitrom.touch4j.client.core.config.XType;
import com.emitrom.touch4j.client.core.handlers.CallbackRegistration;
import com.emitrom.touch4j.client.core.handlers.field.slider.SliderChangeHandler;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayNumber;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.editor.client.IsEditor;
import com.google.gwt.editor.client.LeafValueEditor;
import com.google.gwt.editor.client.adapters.TakesValueEditor;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HasValue;

/**
 * The slider is a way to allow the user to select a value from a given
 * numerical range. You might use it for choosing a percentage, combine two of
 * them to get min and max values, or use three of them to specify the hex
 * values for a color. Each slider contains a single 'thumb' that can be dragged
 * along the slider's length to change the value. Sliders are equally useful
 * inside forms and standalone.
 */
public class Slider extends Field implements HasValue<Double>, IsEditor<LeafValueEditor<Double>> {

	private LeafValueEditor<Double> editor;

	public Slider() {
	}

	protected Slider(final JavaScriptObject jso) {
		super(jso);
	}

	public Slider(final SliderConfig sliderConfig) {
		super(sliderConfig.getJsObj());
	}

	/**
	 * Fires when an option selection has changed.
	 * 
	 * @param handler
	 */
	public CallbackRegistration addChangeHandler(final SliderChangeHandler handler) {
		return this.addWidgetListener(Event.CHANGE.getValue(), handler.getJsoPeer());
	}

	@Override
	public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Double> handler) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LeafValueEditor<Double> asEditor() {
		if (editor == null) {
			editor = TakesValueEditor.of(this);
		}
		return editor;
	}

	@Override
	protected native JavaScriptObject create(JavaScriptObject config) /*-{
		return new $wnd.Ext.field.Slider(config);
	}-*/;

	/**
	 * Returns the value of increment.
	 * 
	 * @return
	 */
	public native double getIncrement() /*-{
		var field = this.@com.emitrom.touch4j.client.core.Component::getOrCreateJsObj()();
		if (field != null) {
			return field.getIncrement();
		}
	}-*/;

	/**
	 * Returns the value of maxValue.
	 * 
	 * @return
	 */
	public native double getMaxValue() /*-{
		var field = this.@com.emitrom.touch4j.client.core.Component::getOrCreateJsObj()();
		if (field != null) {
			return field.getMaxValue();
		}
	}-*/;

	/**
	 * Returns the value of minValue.
	 * 
	 * @return
	 */
	public native double getMinValue() /*-{
		var field = this.@com.emitrom.touch4j.client.core.Component::getOrCreateJsObj()();
		if (field != null) {
			return field.getMinValue();
		}
	}-*/;

	/**
	 * Returns the value of value.
	 * 
	 * @param <T>
	 *            Number/String/Number[]/String[]
	 * @return
	 */
	public native double getSliderValue() /*-{

		var value = 0;
		var field = this.@com.emitrom.touch4j.client.core.Component::getOrCreateJsObj()();

		if (field != null) {

			value = field.getValue()
			var type = typeof value;

			switch (type) {
			case 'number': {
				return value;
			}
			}

			value = field.getValue()[0];
			if (value == undefined) {
				value = 0;
			}

			return value;

		}

	}-*/;

	@Override
	public Double getValue() {
		return getSliderValue();
	}

	@Override
	public String getXType() {
		return XType.SLIDER_FIELD.getValue();
	}

	@Override
	protected native void init()/*-{
		var c = new $wnd.Ext.field.Slider();
		this.@com.emitrom.touch4j.client.core.Component::configPrototype = c.initialConfig;
	}-*/;

	/**
	 * Sets the value of increment.
	 * 
	 * @param value
	 */
	public native void setIncrement(double value) /*-{
		var field = this.@com.emitrom.touch4j.client.core.Component::getOrCreateJsObj()();
		if (field != null) {
			field.setIncrement(value);
		}
	}-*/;

	/**
	 * Sets the value of maxValue.
	 * 
	 * @param value
	 */
	public native void setMaxValue(double value) /*-{
		var field = this.@com.emitrom.touch4j.client.core.Component::getOrCreateJsObj()();
		if (field != null) {
			field.setMaxValue(value);
		}
	}-*/;

	/**
	 * Sets the value of minValue.
	 * 
	 * @param value
	 */
	public native void setMinValue(double value) /*-{
		var field = this.@com.emitrom.touch4j.client.core.Component::getOrCreateJsObj()();
		if (field != null) {
			field.setMinValue(value);
		}
	}-*/;

	/**
	 * Sets the value of value.
	 * 
	 * @param value
	 */
	public native void setSliderValue(double value) /*-{
		var field = this.@com.emitrom.touch4j.client.core.Component::getOrCreateJsObj()();
		if (field != null) {
			field.setValue(value);
		}
	}-*/;

	/**
	 * Sets the value of value.
	 * 
	 * @param value
	 */
	public void setSliderValues(final double[] value) {
		final JsArrayNumber values = JavaScriptObject.createArray().cast();
		for (int i = 0; i < value.length; i++) {
			values.set(i, value[i]);
		}
		setSliderValues(values);
	}

	private native void setSliderValues(JsArrayNumber values) /*-{
		var field = this.@com.emitrom.touch4j.client.core.Component::getOrCreateJsObj()();
		if (field != null) {
			field.setValue(values);
		}
	}-*/;

	private native void setSliderValues(JsArrayString values) /*-{
		var field = this.@com.emitrom.touch4j.client.core.Component::getOrCreateJsObj()();
		if (field != null) {
			field.setValue(values);
		}
	}-*/;

	/**
	 * Sets the value of value.
	 * 
	 * @param value
	 */
	public void setSliderValues(final String[] value) {
		final JsArrayString values = JavaScriptObject.createArray().cast();
		for (int i = 0; i < value.length; i++) {
			values.set(i, value[i]);
		}
		setSliderValues(values);
	}

	@Override
	public void setValue(final Double value) {
		setSliderValue(value);
	}

	@Override
	public void setValue(final Double value, final boolean fireEvents) {
		setSliderValue(value);
	}

}