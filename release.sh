#!/bin/sh
rm -rf dist
ant dist
mvn install:install-file -Dfile=dist/lib/touch4j-4.2-WATCHWORDS-SNAPSHOT.jar -DgroupId=com.emitrom -DartifactId=touch4j -Dversion=4.2-WATCHWORDS-SNAPSHOT -Dpackaging=jar

git checkout --orphan releases
git rm -rf .
rm -rf com
mkdir -p com/emitrom/touch4j
cp -r $HOME/.m2/repository/com/emitrom/touch4j com/emitrom/
mv com/emitrom/touch4j/maven-metadata-local.xml com/emitrom/touch4j/maven-metadata.xml
mv com/emitrom/touch4j/4.2-WATCHWORDS-SNAPSHOT/maven-metadata-local.xml com/emitrom/touch4j/4.2-WATCHWORDS-SNAPSHOT/maven-metadata.xml

git add com*
git commit -m 'Binary Release'
git push origin releases


